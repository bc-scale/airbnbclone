from app import  app
from flask.cli import FlaskGroup
from models import storage


cli = FlaskGroup(app)


@cli.command("create_db")
def create_db():
    """Create database tables"""
    storage.reload()
    storage.close()


if __name__ == "__main__":
    cli()
